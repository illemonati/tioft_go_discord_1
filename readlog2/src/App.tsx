import React, { ChangeEvent, useState, Fragment, useEffect } from 'react';
import './App.css';
import {
    Button,
    Container,
    Grid,
    List,
    ListItem,
    ListItemText,
    Paper,
    TextField,
    Typography,
} from '@material-ui/core';
import { getLogEntries } from './utils/LogEntryUtils';
import { LogEntry } from './utils/LogEntryFormat';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { Pagination } from '@material-ui/lab';
import SingleEntryDisplay from './modules/SingleEntryDisplay';
import SnackBarInfoComponent from './modules/SnackBarInfoComponent';

function App() {
    const [logPath, setLogPath] = useState('../log.txt');
    const handleLogPathChange = (e: ChangeEvent<HTMLInputElement>) => {
        const newPath = e.currentTarget.value;
        setLogPath(newPath);
    };
    const [logEntries, setLogEntries] = useState([] as LogEntry[]);
    const [filteredEntries, setFilteredEntries] = useState([] as LogEntry[]);
    const [page, setPage] = useState(1);
    const [numberOfPages, setNumberOfPages] = useState(20);
    const [snackBarMessage, setSnackBarMessage] = useState('');
    const [pageSize, setPageSize] = useState(10);
    const [userIDFilter, setUserIdFilter] = useState('');
    const [channelIDFilter, setchannelIDFilter] = useState('');
    const [entryOpenStates, setEntryOpenStates] = useState([] as boolean[]);
    const loadLogEntries = async () => {
        setSnackBarMessage('Loading Log File');
        const entries = await getLogEntries(logPath);
        setLogEntries(entries.reverse());
        setNumberOfPages(Math.floor(entries.length / pageSize));
        setSnackBarMessage('');
    };

    const handlePageSizeChange = (e: ChangeEvent<HTMLInputElement>) => {
        const newSize = e.currentTarget.value;
        try {
            const n = parseInt(newSize);
            setPageSize(n);
        } catch (e) {}
    };

    const handleUserIDFilterChange = (e: ChangeEvent<HTMLInputElement>) => {
        const newUserIDFilter = e.currentTarget.value;
        setUserIdFilter(newUserIDFilter);
    };

    const handleChannelIDFilerChange = (e: ChangeEvent<HTMLInputElement>) => {
        const newChannelIDFiler = e.currentTarget.value;
        setchannelIDFilter(newChannelIDFiler);
    };

    useEffect(() => {
        const filterChannel = channelIDFilter.length > 0;
        const filterUser = userIDFilter.length > 0;
        setFilteredEntries(
            logEntries.filter((e) => {
                let allowed = true;
                if (filterChannel) {
                    allowed = allowed && e.channel_id === channelIDFilter;
                }
                if (filterUser) {
                    allowed = allowed && e.author.id === userIDFilter;
                }
                return allowed;
            })
        );
    }, [logEntries, channelIDFilter, userIDFilter]);

    useEffect(() => {
        setNumberOfPages(Math.floor(filteredEntries.length / pageSize));
    }, [filteredEntries, pageSize]);

    useEffect(() => {
        setEntryOpenStates([] as boolean[]);
    }, [filteredEntries, pageSize, page]);

    const pagination = (
        <Fragment>
            <Container>
                <Grid container alignItems="center" justify="center">
                    <Grid item alignItems="center" justify="center">
                        <Pagination
                            count={numberOfPages}
                            variant="outlined"
                            boundaryCount={2}
                            siblingCount={3}
                            page={page}
                            onChange={(e, v) => setPage((_) => v)}
                        />
                    </Grid>
                </Grid>
            </Container>
            <br />
            <br />
        </Fragment>
    );

    return (
        <div className="App">
            <Container>
                <Typography variant="h3" color="textPrimary">
                    Read Log
                </Typography>
                <br />
                <br />
                <Paper>
                    <br />
                    <br />
                    <Container>
                        <Grid container>
                            <Grid item xs={10}>
                                <TextField
                                    fullWidth
                                    color="primary"
                                    value={logPath}
                                    label="Log Path"
                                    onChange={handleLogPathChange}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <Button
                                    variant="outlined"
                                    onClick={() => loadLogEntries().then()}
                                >
                                    Load
                                </Button>
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container justify="center" spacing={3}>
                                    <Grid
                                        item
                                        xs={4}
                                        style={{ marginTop: '2%' }}
                                        spacing={3}
                                    >
                                        <TextField
                                            fullWidth
                                            color="primary"
                                            value={pageSize}
                                            label="Page Size"
                                            onChange={handlePageSizeChange}
                                        />
                                    </Grid>
                                    <Grid
                                        item
                                        xs={4}
                                        style={{ marginTop: '2%' }}
                                        spacing={3}
                                    >
                                        <TextField
                                            fullWidth
                                            color="primary"
                                            value={userIDFilter}
                                            label="UserID Filter"
                                            onChange={handleUserIDFilterChange}
                                        />
                                    </Grid>
                                    <Grid
                                        item
                                        xs={4}
                                        style={{ marginTop: '2%' }}
                                        spacing={3}
                                    >
                                        <TextField
                                            fullWidth
                                            color="primary"
                                            value={channelIDFilter}
                                            label="ChannelID Filter"
                                            onChange={
                                                handleChannelIDFilerChange
                                            }
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid
                                item
                                xs={12}
                                style={{ textAlign: 'left', marginTop: '3%' }}
                            >
                                <Typography variant="subtitle1">
                                    Press Load to view logs
                                </Typography>
                            </Grid>
                            <Grid item xs={12} style={{ textAlign: 'left' }}>
                                <Typography variant="subtitle1">
                                    Note: You can click on fields to copy their
                                    values
                                </Typography>
                            </Grid>
                        </Grid>
                    </Container>
                    <br />
                    <br />
                </Paper>
                <br />
                <br />
                {logEntries.length > 0 && (
                    <Fragment>
                        {pagination}
                        <Paper>
                            <br />
                            <br />
                            <Container>
                                <List>
                                    {filteredEntries
                                        .slice(
                                            (page - 1) * pageSize,
                                            page * pageSize
                                        )
                                        .map((entry, i) => {
                                            let primaryText =
                                                entry.content.length > 0
                                                    ? entry.content
                                                    : 'empty message';
                                            entry.attachments.length > 0 &&
                                                (primaryText += ` - ${entry.attachments.length} attachments`);
                                            entry.embeds.length > 0 &&
                                                (primaryText += ` - ${entry.embeds.length} embeds`);
                                            const secondaryText = `${entry.author.username}#${entry.author.discriminator} - ${entry.timestamp}`;

                                            return (
                                                <Fragment key={i}>
                                                    <ListItem
                                                        button
                                                        onClick={() => {
                                                            setEntryOpenStates(
                                                                (states) => {
                                                                    states[
                                                                        i
                                                                    ] = !states[
                                                                        i
                                                                    ];
                                                                    return states.slice();
                                                                }
                                                            );
                                                        }}
                                                    >
                                                        <ListItemText
                                                            primary={
                                                                primaryText
                                                            }
                                                            secondary={
                                                                secondaryText
                                                            }
                                                        />
                                                        {entryOpenStates[i] ? (
                                                            <ExpandLess />
                                                        ) : (
                                                            <ExpandMore />
                                                        )}
                                                    </ListItem>
                                                    <SingleEntryDisplay
                                                        entry={entry}
                                                        open={
                                                            entryOpenStates[i]
                                                        }
                                                        setSnackBarMessage={
                                                            setSnackBarMessage
                                                        }
                                                    />
                                                </Fragment>
                                            );
                                        })}
                                </List>
                            </Container>
                            <br />
                            <br />
                        </Paper>
                        <br />
                        <br />
                        {pagination}
                    </Fragment>
                )}
            </Container>
            <SnackBarInfoComponent message={snackBarMessage} />
        </div>
    );
}

export default App;
