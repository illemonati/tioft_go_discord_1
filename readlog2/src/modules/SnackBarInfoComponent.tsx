import {Snackbar} from "@material-ui/core";
import React from "react";

interface SnackBarInfoComponentProps {
    message: string
}

export default function SnackBarInfoComponent(props: SnackBarInfoComponentProps) {
    return (
        <Snackbar
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            open={props.message.length > 0}
            message={props.message}
        />
    )
}


