import {LogEntry} from "../utils/LogEntryFormat";
import React, {Fragment, useState} from "react";
import {Collapse, List, ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import ExpandLess from "@material-ui/icons/ExpandLess";
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import ExpandMore from "@material-ui/icons/ExpandMore";

interface SingleEntryProps {
    entry: LogEntry;
    open: boolean;
    recursiveDepth?: number;
    setSnackBarMessage?: (msg: string) => void;
    isAttachment?: boolean;
}

export default function SingleEntryDisplay(props: SingleEntryProps) {
    const entry = props.entry;
    const open = props.open;
    const recursiveDepth = props.recursiveDepth || 1;
    const isAttachment = props.isAttachment || false;
    const [subListStates, setSubListStates] = useState([] as boolean[]);
    const setSnackBarMessage = props.setSnackBarMessage;
    const handleOpenLink = (link: string) => {
        window.open(link, '_blank');
    }
    const handleOpenBackupLink = (link: string) => {
        const rg = /(\d+)\/(.+)/;
        const match = link.match(rg);
        if (match) {
            const backupLink = '/attachments/' + match[match.length - 1];
            window.open(backupLink, '_blank');
        }
    }


    return (
        <Fragment>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    {Object.keys(entry).map((field, i) => {
                        //@ts-ignore
                        const fieldContent = entry[field];
                        const fieldContentIsObject = typeof fieldContent === 'object' && fieldContent !== null;
                        let fieldContentIsAttachment = false;
                        if (isAttachment || field === 'attachments') {
                            fieldContentIsAttachment = true;
                        }
                        let fieldContentIsURL = false;
                        try {
                            new URL(fieldContent);
                            fieldContentIsURL = true;
                        } catch (e) {}
                        const itemStyle = {
                            paddingLeft: (recursiveDepth * 5) + 'vw'
                        }
                        let secondary = '';
                        if (!fieldContentIsObject) {
                            if (typeof fieldContent === 'string' || typeof fieldContent === 'number') {
                                secondary = fieldContent.toString();
                            } else {
                                secondary = JSON.stringify(fieldContent);
                            }
                        }

                        return (
                            <Fragment key={i}>
                                <ListItem button
                                          style={itemStyle}
                                          onClick={() => {
                                              if (fieldContentIsObject) {
                                                  setSubListStates(states => {
                                                      states[i] = !states[i];
                                                      return states.slice();
                                                  })
                                              } else {
                                                  let sbm = '';
                                                  navigator.clipboard.writeText(secondary).then(() => {
                                                      sbm = 'Copied';
                                                  }).catch((e) => {
                                                      console.log(e)
                                                      sbm = 'Copy Error';
                                                  }).finally(() => {
                                                      if (setSnackBarMessage) {
                                                          setSnackBarMessage(sbm);
                                                          setTimeout(() => {
                                                              setSnackBarMessage('');
                                                          }, 3000);
                                                      }
                                                  });
                                              }
                                          }}
                                >
                                    <ListItemText primary={field}
                                                  secondary={secondary}
                                    />
                                    {
                                        (fieldContentIsURL && fieldContentIsAttachment) && (
                                            <ListItemIcon onClick={_ => handleOpenBackupLink(fieldContent)}>
                                                <OpenInNewIcon />
                                            </ListItemIcon>
                                        )
                                    }
                                    {fieldContentIsURL && (
                                        <ListItemIcon onClick={_ => handleOpenLink(fieldContent)}>
                                            <OpenInNewIcon />
                                        </ListItemIcon>
                                    )}
                                    {fieldContentIsObject && (subListStates[i] ? <ExpandLess/> : <ExpandMore/>)}
                                </ListItem>
                                {fieldContentIsObject &&
                                <SingleEntryDisplay entry={fieldContent} open={subListStates[i]}
                                                    isAttachment={fieldContentIsAttachment}
                                                    recursiveDepth={recursiveDepth + 1}/>
                                }
                            </Fragment>
                        )
                    })}
                </List>
            </Collapse>
        </Fragment>
    )
}