import {LogEntry} from "../utils/LogEntryFormat";


export function logEntriesReducer(state = [], action: any): LogEntry[] {
    switch (action.type) {
        case 'UPDATE_LOG_ENTRIES':
            return [...action.payload];
        case 'ADD_LOG_ENTRY':
            const newState = state.slice()
            // @ts-ignore
            newState.push({...action.payload});
            return newState;
        default:
            return state;
    }
}