
import {combineReducers} from 'redux';
import {logEntriesReducer} from "./logEntries";


const rootReducer = combineReducers({
    logEntries: logEntriesReducer,
});

export default rootReducer;
