import {LogEntry} from "./LogEntryFormat";


export const getLog = async (url: string): Promise<string> => {
    let headers = new Headers({
        'Content-Type': 'text/plain'
    });
    const resp = await fetch(url, {
        method: 'GET',
        headers: headers
    });
    return await resp.text()
}

export const getLogEntries = async(url: string): Promise<LogEntry[]> => {
    const log = await getLog(url);
    const entriesOg = log.split('\n');
    // const entriesOg = log.split(/.*(?=(\{\"id\".+\"cha))/g).slice(0, 50);

    const entries = [] as LogEntry[];
    entriesOg.forEach((e) => {
        try {
            const obj = JSON.parse(e);
            entries.push(obj);
        } catch (e) {
            console.log(e);
        }
    })
    return entries;
}
