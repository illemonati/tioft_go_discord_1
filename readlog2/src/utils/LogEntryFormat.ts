export interface Author {
    id: string;
    email: string;
    username: string;
    avatar: string;
    locale: string;
    discriminator: string;
    token: string;
    verified: boolean;
    mfa_enabled: boolean;
    bot: boolean;
    [key: string]: any;
}

export interface Attachment {
    id: string;
    url: string;
    proxy_url: string;
    filename: string;
    width: number;
    height: number;
    size: number;
    [key: string]: any;
}

export interface Image {
    url: string;
    proxy_url: string;
    [key: string]: any;
}

export interface Field {
    name: string;
    value: string;
    [key: string]: any;
}

export interface Embed {
    type: string;
    title: string;
    color?: number;
    image: Image;
    fields: Field[];
    [key: string]: any;
}

export interface Member {
    guild_id: string;
    joined_at: Date;
    nick: string;
    deaf: boolean;
    mute: boolean;
    user?: any;
    roles: string[];
    premium_since: string;
    [key: string]: any;
}

export interface LogEntry {
    id: string;
    channel_id: string;
    guild_id: string;
    content: string;
    timestamp: Date;
    edited_timestamp: string;
    mention_roles: any[];
    tts: boolean;
    mention_everyone: boolean;
    author: Author;
    attachments: Attachment[];
    embeds: Embed[];
    mentions: any[];
    reactions?: any;
    pinned: boolean;
    type: number;
    webhook_id: string;
    member: Member;
    mention_channels?: any;
    activity?: any;
    application?: any;
    message_reference?: any;
    flags: number;
    [key: string]: any;
}


