import {LogEntry} from "../utils/LogEntryFormat";


export function updateLogEntries(newEntries: LogEntry[]) {
    return {
        type: 'UPDATE_LOG_ENTRIES',
        payload: newEntries,
    }
}

export function addLogEntry(newEntry: LogEntry) {
    return {
        type: 'ADD_LOG_ENTRY',
        payload: newEntry
    }
}
