
exename := "tioft_go_discord_1"
buildDir := "build"

.ONESHELL: build run
.PHONY: build
build:
	mkdir -p ${buildDir}
	go get -u
	go build -ldflags="-w -s" -o ./${buildDir}/${exename} "."
	cp -r ./assets/ ./${buildDir}/assets/
	cp -r ./readlog/ ./${buildDir}/readlog/
	cp -r ./readlog2/build/ ./${buildDir}/readlog2

run:
	cd ${buildDir}
	./${exename}

clean:
	rm -f ./${buildDir}/${exename}
	rm -rf ./${buildDir}/assets/
	rm -rf ./${buildDir}/readlog/
	rm -rf ./${buildDir}/readlog2

all: build run

default: build
	
