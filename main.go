package main

import (
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/handlers"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

var selfBot = true

func main() {
	token := os.Getenv("DISCORD_TOKEN")
	if len(token) < 1 {
		log.Fatal("Please set the DISCORD_TOKEN env variable to your discord token")
	}

	if strings.HasPrefix(token, "Bot ") {
		selfBot = false
	}

	discord, err := discordgo.New(token)
	if err != nil {
		log.Fatal("Unable to start client", err)
		return
	}
	discord.UserAgent = "TIOFT"
	discord.AddHandler(messageCreate)
	discord.AddHandler(handlers.SlashCommandHandler)

	err = discord.Open()
	if err != nil {
		log.Fatal("error opening connection,", err)
		return
	}
	log.Println(discord.State.User.String())

	handlers.RegisterSlashCommand(discord)

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	discord.Close()
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Content == "" && selfBot {
		chanMsgs, err := s.ChannelMessages(m.ChannelID, 1, "", "", m.ID)
		if err != nil {
			log.Printf("unable to get messages for self bot: %s", err)
			return
		}
		m = &discordgo.MessageCreate{
			Message: chanMsgs[0],
		}
	}
	go handlers.Dancechar(s, m)
	go handlers.Ping(s, m)
	go handlers.Alex(s, m)
	go handlers.Beter(s, m)
	go handlers.BestGirl(s, m)
	go handlers.Avatar(s, m)
	go handlers.Scp(s, m)
	go handlers.Communism(s, m)
	go handlers.NHentai(s, m)
	go handlers.TheBestImageEverCreated(s, m)
	go handlers.Youtube(s, m)
	go handlers.SystemInfo(s, m)
	go handlers.DogeLore(s, m)
	go handlers.Greentext(s, m)
	go handlers.CursedWhy(s, m)
	go handlers.Obama(s, m)
	go handlers.DontUseMyEmoji(s, m)
	go handlers.Emoji(s, m)
	go handlers.Someone(s, m)
	go handlers.DamnedSmile(s, m)
	go handlers.AntiType(s, m)
	go handlers.RandomCaps(s, m)
	go handlers.Image(s, m)
	go handlers.Latency(s, m)
	go utils.LogMessage(m, true)
}
