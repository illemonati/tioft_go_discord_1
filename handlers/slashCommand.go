package handlers

import "github.com/bwmarrin/discordgo"

// RegisterSlashCommand : gives a message when a slash command is used
func RegisterSlashCommand(s *discordgo.Session) {
	command := discordgo.ApplicationCommand{
		Name:        "slash",
		Description: "objective information regarding slash commands",
	}
	s.ApplicationCommandCreate(s.State.User.ID, "", &command)
}

func SlashCommandHandler(s *discordgo.Session, i *discordgo.InteractionCreate) {
	s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: "Slash Command Bad",
		},
	})
}
