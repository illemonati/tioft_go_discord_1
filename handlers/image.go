package handlers

import (
	"bytes"
	"fmt"
	"image"
	"image/png"

	"github.com/bwmarrin/discordgo"
	"github.com/disintegration/imaging"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

type ImageCommand struct {
	CommandName        string
	CommandDescription string
	CommandFunction    func(image.Image) image.Image
}

var encoder = png.Encoder{
	CompressionLevel: -3,
}

var imageCommands = map[string]*ImageCommand{
	"invert": {
		CommandName:        "invert",
		CommandDescription: "inverts the image",
		CommandFunction: func(image image.Image) image.Image {
			return imaging.Invert(image)
		},
	},
	"grayscale": {
		CommandName:        "grayscale",
		CommandDescription: "turns the image into grayscale",
		CommandFunction: func(image image.Image) image.Image {
			return imaging.Grayscale(image)
		},
	},
	"flipH": {
		CommandName:        "flipH",
		CommandDescription: "flips the image horizontally",
		CommandFunction: func(image image.Image) image.Image {
			return imaging.FlipH(image)
		},
	},
	"flipV": {
		CommandName:        "flipV",
		CommandDescription: "flips the image vertically",
		CommandFunction: func(image image.Image) image.Image {
			return imaging.FlipV(image)
		},
	},
}

func Image(s *discordgo.Session, m *discordgo.MessageCreate) {
	if !utils.IsCommand(m, "image") {
		return
	}

	if utils.GetMessageFirstArgument(m) == "help" {
		embed := new(discordgo.MessageEmbed)
		embed.Title = "Image commands"
		embed.Description = "List of commands, chain as many as you want in sequence"
		embed.Fields = []*discordgo.MessageEmbedField{}
		for _, command := range imageCommands {
			field := &discordgo.MessageEmbedField{
				Name:   command.CommandName,
				Value:  command.CommandDescription,
				Inline: false,
			}
			embed.Fields = append(embed.Fields, field)
		}
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
		return
	}

	attachments := m.Attachments

	if len(attachments) < 1 {
		s.ChannelMessageSend(m.ChannelID, "Please input the image as the first attachment")
		return
	}

	s.ChannelMessageSend(m.ChannelID, "Downloading")

	imagePath := utils.AttachmentBaseDirToFilePath("cache/image/", attachments[0])
	imageDownloadResult := utils.DownloadAttachment(m, 0, imagePath)
	if imageDownloadResult.Error != nil {
		s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Error downloading Image %s", imageDownloadResult.Error.Error()))
		return
	}

	imagingImage, err := imaging.Open(imageDownloadResult.FilePath)
	if err != nil {
		s.ChannelMessageSend(m.ChannelID, "Error Opening Image")
	}

	s.ChannelMessageSend(m.ChannelID, "Processing")
	arguments := utils.GetMessageArguments(m)[1:]
	for i, argument := range arguments {
		if imageCommands[argument] != nil {
			imagingImage = imageCommands[argument].CommandFunction(imagingImage)
		} else {
			s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Invalid command at position %d (%s), please input a valid command", i, argument))
		}
	}
	sendOutputImage(s, m, imagingImage)
}

func sendOutputImage(s *discordgo.Session, m *discordgo.MessageCreate, image image.Image) error {
	buffer := bytes.NewBuffer(nil)
	encoder.Encode(buffer, image)
	err := png.Encode(buffer, image)
	if err != nil {
		s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Png Encode Error: %s", err.Error()))
	}

	if err != nil {
		return err
	}
	file := &discordgo.File{
		Name:        "output.png",
		ContentType: "image/png",
		Reader:      buffer,
	}
	_, err = s.ChannelMessageSendComplex(m.ChannelID, &discordgo.MessageSend{
		Content: "",
		Embed:   nil,
		TTS:     false,
		Files:   []*discordgo.File{file},
	})
	if err != nil {
		s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Send error: %s", err.Error()))
	}
	return err
}
