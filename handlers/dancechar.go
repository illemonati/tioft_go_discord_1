package handlers

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

func Dancechar(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "dancechar") {
		acceptableChars := []string{
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p",
			"q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
			"6", "7", "8", "9",
		}

		firstArg := utils.GetMessageFirstArgument(m)
		in := false
		for _, char := range acceptableChars {
			if firstArg == char {
				in = true
			}
		}
		if in {
			embed := new(discordgo.MessageEmbed)
			image := new(discordgo.MessageEmbedImage)
			image.URL = fmt.Sprintf("http://dance.cavifax.com/images/%s.gif", firstArg)
			embed.Image = image
			s.ChannelMessageSendEmbed(m.ChannelID, embed)
		} else {
			fmt.Println(3)
			_, err := s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("The argument %s is not supported !", firstArg))
			fmt.Println(err.Error())
		}
	}
}
