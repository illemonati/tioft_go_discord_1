package handlers

import (
	"fmt"
	"log"
	"path"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/parnurzeal/gorequest"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

var savedChannels []string

func NHentai(s *discordgo.Session, m *discordgo.MessageCreate) {

	insaved := false
	for _, channel := range savedChannels {
		if m.ChannelID == channel {
			insaved = true
			break
		}
	}

	if utils.IsCommand(m, "nh") || insaved {
		args := utils.GetMessageArguments(m)
		for _, arg := range args {
			n, err := strconv.Atoi(arg)
			if err != nil {
				continue
			}
			go sendNHentais(s, m, n)
		}
	}
	if utils.IsCommand(m, "nhon") {
		savedChannels = append(savedChannels, m.ChannelID)
		log.Printf("nh on in channel %s", m.ChannelID)
		s.ChannelMessageSend(m.ChannelID, "nhon")
	}
	if utils.IsCommand(m, "nhoff") {
		toRemove := -1
		for i, channel := range savedChannels {
			if channel == m.ChannelID {
				toRemove = i
			}
		}
		if toRemove != -1 {
			savedChannels[toRemove] = savedChannels[len(savedChannels)-1]
			savedChannels = savedChannels[:len(savedChannels)-1]
		}
		return
	}
	if utils.IsCommand(m, "nhr") {
		n := getRandomNHentaiNumber()
		if n != -1 {
			sendNHentais(s, m, n)
		}
		return
	}
	if utils.IsCommand(m, "nhq") {
		querylist := utils.GetMessageArguments(m)
		if len(querylist) < 2 {
			return
		}
		query := strings.Join(querylist[1:], " ")
		numbers := getNumbersFromQuery(query)
		for _, number := range numbers {
			go sendNHentais(s, m, number)
			time.Sleep(1 * time.Second)
		}
		return
	}

}

func sendNHentais(s *discordgo.Session, m *discordgo.MessageCreate, n int) {
	nhn := newNhentai(n)
	nhn.getInfo()
	em := new(discordgo.MessageEmbed)
	em.Title = fmt.Sprintf("%s (%d)", nhn.title, nhn.number)
	emDescription := new(discordgo.MessageEmbedField)
	emDescription.Name = "Description"
	emDescription.Value = nhn.description
	emTags := new(discordgo.MessageEmbedField)
	emTags.Name = "Tags"
	emTags.Value = nhn.tags
	em.URL = nhn.url
	emImage := new(discordgo.MessageEmbedImage)
	emImage.URL = nhn.imageURL
	em.Image = emImage
	em.Fields = []*discordgo.MessageEmbedField{emDescription, emTags}
	go s.ChannelMessageSendEmbed(m.ChannelID, em)
}

type NHentaiNovel struct {
	title, tags, imageURL, description, url string
	number                                  int
}

func newNhentai(n int) *NHentaiNovel {
	nhentaiNovel := new(NHentaiNovel)
	nhentaiNovel.number = n
	nhentaiNovel.url = fmt.Sprintf("https://nhentai.net/g/%d", nhentaiNovel.number)
	nhentaiNovel.getInfo()
	return nhentaiNovel
}

func (nhn *NHentaiNovel) getInfo() {
	_, body, _ := gorequest.New().Get(nhn.url).End()
	wg := new(sync.WaitGroup)
	wg.Add(4)
	go nhn.getDescription(body, wg)
	go nhn.getImageURL(body, wg)
	go nhn.getTags(body, wg)
	go nhn.getTitle(body, wg)
	wg.Wait()
}

func (nhn *NHentaiNovel) getTitle(body string, wg *sync.WaitGroup) {
	reg := regexp.MustCompile(`before">(.*)<\/span>.*h1>`)
	captures := reg.FindStringSubmatch(body)
	if len(captures) >= 1 {
		nhn.title = utils.HtmlToText(captures[1])
	} else {
		nhn.title = "Not found"
	}
	wg.Done()
}

func (nhn *NHentaiNovel) getDescription(body string, wg *sync.WaitGroup) {
	reg := regexp.MustCompile(`.*<h2 class="title">.*before">(.*)<\/span>.*h2>`)
	captures := reg.FindStringSubmatch(body)
	if len(captures) >= 1 {
		nhn.description = utils.HtmlToText(captures[1])
	} else {
		nhn.description = "N/A"
	}
	wg.Done()
}

func (nhn *NHentaiNovel) getTags(body string, wg *sync.WaitGroup) {
	reg := regexp.MustCompile(`<meta name="twitter:description" content="(.+)" /><meta.*"v`)
	captures := reg.FindStringSubmatch(body)
	if len(captures) >= 1 {
		nhn.tags = utils.HtmlToText(captures[1])
	} else {
		nhn.tags = "N/A"
	}
	wg.Done()
}

func (nhn *NHentaiNovel) getImageURL(body string, wg *sync.WaitGroup) {
	reg := regexp.MustCompile(`img class="lazyload.*data-src="(.+)"\s+s`)
	captures := reg.FindStringSubmatch(body)
	if len(captures) >= 1 {
		nhn.imageURL = utils.HtmlToText(captures[1])
	} else {
		nhn.imageURL = ""
	}
	wg.Done()
}

func getRandomNHentaiNumber() int {
	resp, _, errs := gorequest.New().Get("https://nhentai.net/random").End()
	if errs != nil {
		return -1
	}
	n := path.Base((resp.Request.URL.String()))
	ni, err := strconv.Atoi(n)
	if err != nil {
		return -1
	}
	return ni
}

func getNumbersFromQuery(query string) (results []int) {
	_, body, errs := gorequest.New().Get(fmt.Sprintf("https://nhentai.net/search/?q=%s&sort=popular", query)).End()
	if errs != nil {
		return
	}
	reg := regexp.MustCompile(`g\/(\d+)`)
	matchs := reg.FindAllStringSubmatch(body, 5)
	for _, match := range matchs {
		number, err := strconv.Atoi(match[1])
		if err != nil {
			continue
		}
		results = append(results, number)
	}
	return
}
