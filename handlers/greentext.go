package handlers

import (
	"github.com/bwmarrin/discordgo"
	"github.com/turnage/graw"
	"github.com/turnage/graw/reddit"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
	"log"
	"time"
)

var greentextHandler *GreentextHandler = new(GreentextHandler)

type GreentextHandler struct {
	discordSession  *discordgo.Session
	discordChannels []string
	isRunning       bool
}

func (gth *GreentextHandler) Post(post *reddit.Post) error {
	if post.URL != "" {
		for _, dc := range gth.discordChannels {
			_, err := gth.discordSession.ChannelMessageSendEmbed(dc, &discordgo.MessageEmbed{
				Title: post.Title,
				Image: &discordgo.MessageEmbedImage{
					URL: post.URL,
				},
			})
			if err != nil {
				log.Println(err)
			}
		}
	}
	return nil
}

func Greentext(s *discordgo.Session, m *discordgo.MessageCreate) {

	greentextHandler.discordSession = s
	if !greentextHandler.isRunning {
		apiHandle, err := reddit.NewScript("greentext", 5*time.Second)
		if err != nil {
			log.Println(err)
			return
		}
		cfg := graw.Config{Subreddits: []string{"greentext"}}
		_, wait, err := graw.Scan(greentextHandler, apiHandle, cfg)
		if err != nil {
			log.Println(err)
			return
		}
		go wait()
		greentextHandler.isRunning = true
		println("greentext on")
	}

	insaved := false
	for _, c := range greentextHandler.discordChannels {
		if c == m.ChannelID {
			insaved = true
		}
	}
	if utils.IsCommand(m, "greentextOn") {
		if !insaved {
			greentextHandler.discordChannels = append(greentextHandler.discordChannels, m.ChannelID)
			log.Printf("greentextOn in channel %s", m.ChannelID)
			s.ChannelMessageSend(m.ChannelID, "greentextOn")
		}
	}

	if utils.IsCommand(m, "greentextOff") {
		toRemove := -1
		for i, channel := range greentextHandler.discordChannels {
			if channel == m.ChannelID {
				toRemove = i
			}
		}
		if toRemove != -1 {
			greentextHandler.discordChannels[toRemove] = greentextHandler.discordChannels[len(greentextHandler.discordChannels)-1]
			greentextHandler.discordChannels = greentextHandler.discordChannels[:len(greentextHandler.discordChannels)-1]
		}
		return
	}

}
