package handlers

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

func Communism(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "communism") {
		communismText := "A spectre is haunting Europe — the spectre of communism. All the powers of old Europe have entered into a holy alliance to exorcise this spectre: Pope and Tsar, Metternich and Guizot, French Radicals and German police-spies.Where is the party in opposition that has not been decried as communistic by its opponents in power? Where is the opposition that has not hurled back the branding reproach of communism, against the more advanced opposition parties, as well as against its reactionary adversaries?Two things result from this fact:I. Communism is already acknowledged by all European powers to be itself a power.II. It is high time that Communists should openly, in the face of the whole world, publish their views, their aims, their tendencies, and meet this nursery tale of the Spectre of Communism with a manifesto of the party itself.To this end, Communists of various nationalities have assembled in London and sketched the following manifesto, to be published in the English, French, German, Italian, Flemish and Danish languages"
		communismURL := "https://www.marxists.org/archive/marx/works/1848/communist-manifesto/ch01.htm"
		imgURL := "https://images2.alphacoders.com/743/74302.jpg"
		em := new(discordgo.MessageEmbed)
		em.Title = "Manifesto of the Communist Party"
		em.URL = communismURL
		em.Description = communismText
		emimg := new(discordgo.MessageEmbedImage)
		emimg.URL = imgURL
		em.Image = emimg
		em.Color = 0xcc0000
		s.ChannelMessageSendEmbed(m.ChannelID, em)
	}
}
