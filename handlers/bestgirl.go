package handlers

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

func BestGirl(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.MessageContains(m, "bestgirl") {
		imgu := "https://vignette.wikia.nocookie.net/haruhi/images/2/28/SuzumiyaHaruhi_Char2.jpg/revision/latest?cb=20171012164721"
		em := new(discordgo.MessageEmbed)
		emimg := new(discordgo.MessageEmbedImage)
		emimg.URL = imgu
		em.Image = emimg
		em.Title = "Best Female"
		s.ChannelMessageSendEmbed(m.ChannelID, em)
	}
}
