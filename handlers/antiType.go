package handlers

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

var antiTypeChannels = map[string]struct{}{}

// AntiType : Handler for AntiType to stop people from typing
func AntiType(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "antitype") {

		if utils.GetMessageFirstArgument(m) == "start" {
			// antiTypeChannels = append(antiTypeChannels, m.ChannelID)
			antiTypeChannels[m.ChannelID] = struct{}{}
			s.MessageReactionAdd(m.ChannelID, m.ID, "🍎")
		}
		if utils.GetMessageFirstArgument(m) == "stop" {
			delete(antiTypeChannels, m.ChannelID)
			s.MessageReactionAdd(m.ChannelID, m.ID, "🍎")
		}
		return
	}
	if _, ok := antiTypeChannels[m.ChannelID]; ok {
		s.ChannelMessageDelete(m.ChannelID, m.ID)
	}
}
