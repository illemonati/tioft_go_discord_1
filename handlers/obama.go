package handlers

import (
	"log"
	"path/filepath"
	"regexp"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

var obamaList = map[string]string{
	"prisim":        "obama-prisim.gif",
	"solder":        "obama-solder.png",
	"multisphere":   "obama-multisphere.mp4",
	"tesseract":     "obama-tesseract.png",
	"spawner":       "obama-spawner.jpg",
	"sphere":        "obama-sphere.gif",
	"rhombus":       "obama-rhombus.png",
	"flag":          "obama-flag.png",
	"dodecahedreon": "obama-dodecahedreon.gif",
	"globe":         "obama-globe.jpg",
	"boomarang":     "obama-rang.jpg",
	"cube":          "obama-cube.jpg",
	"rubiks-cube":   "obama-rubiks-cube.mp4",
	"ultimate":      "obama-ultimate.mp4",
	"country":       "obama-country.jpg",
	"hands":         "obama-hands.jpg",
	"broccoli":      "obama-broccoli.jpg",
	"fire":          "obama-fire.jpg",
	"head":          "obama-head.jpg",
	"light":         "obama-light.jpg",
	"love":          "obama-love.jpg",
	"tarts":         "obama-tarts.jpg",
	"tree":          "obama-tree.jpg",
	"hits-earth":    "obama-hits-earth.gif",
}

var obamaImageExtensionRegex = regexp.MustCompile(`(jpg|png|jpeg|gif)`)

// Obama : Handler for Obama Arsenal
func Obama(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "obama") {
		name := ""

		if len(utils.GetMessageArguments(m)) < 2 {
			go obamaHelp(s, m)
			return
		}

		firstArg := utils.GetMessageFirstArgument(m)

		for key := range obamaList {
			if key == firstArg {
				name = key
				break
			}
		}

		if len(name) == 0 {
			go obamaHelp(s, m)
			return
		}

		go processObama(s, m, name)
	}
}

func processObama(s *discordgo.Session, m *discordgo.MessageCreate, name string) {
	fileName := obamaList[name]
	fileExtension := filepath.Ext(fileName)
	isImage := false
	if obamaImageExtensionRegex.MatchString(fileExtension) {
		isImage = true
	}
	embed := createObamaEmbed(name, fileName, fileExtension, isImage)
	files, err := createObamaFiles(fileName, fileExtension, isImage)
	if err != nil {
		log.Println(err)
		return
	}
	ms := discordgo.MessageSend{
		Content: "",
		Embed:   embed,
		TTS:     true,
		Files:   files,
	}
	s.ChannelMessageSendComplex(m.ChannelID, &ms)
}

func createObamaEmbed(name, fileName, fileExtension string, isImage bool) (embed *discordgo.MessageEmbed) {
	embed = new(discordgo.MessageEmbed)
	embed.Title = name

	if isImage {
		embed.Image = new(discordgo.MessageEmbedImage)
		embed.Image.URL = "attachment://" + fileName
	} else {
		embed.Video = new(discordgo.MessageEmbedVideo)
		embed.Video.URL = "attachment://" + fileName
	}

	return
}

func createObamaFiles(fileName, fileExtension string, isImage bool) (files []*discordgo.File, err error) {
	var obamaContent *discordgo.File

	if isImage {
		obamaContent, err = utils.GetAsset(fileName, "image/"+fileExtension)
	} else {
		obamaContent, err = utils.GetAsset(fileName, "video/"+fileExtension)
	}

	if err != nil {
		return
	}
	files = append(files, obamaContent)
	return
}

func obamaHelp(s *discordgo.Session, m *discordgo.MessageCreate) {
	embed := new(discordgo.MessageEmbed)
	embed.Title = "Obama Help"

	usageField := new(discordgo.MessageEmbedField)
	usageField.Name = "Usage"
	usageField.Value = utils.PREFIX + "obama <weapon name>"

	weaponNamesField := new(discordgo.MessageEmbedField)
	weaponNamesField.Name = "Weapon Names"
	weaponNamesField.Value = ""
	for name := range obamaList {
		weaponNamesField.Value += name + "\n"
	}

	embed.Fields = []*discordgo.MessageEmbedField{
		usageField,
		weaponNamesField,
	}

	s.ChannelMessageSendEmbed(m.ChannelID, embed)
}
