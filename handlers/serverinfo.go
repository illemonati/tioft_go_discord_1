package handlers

import (
	"fmt"
	"time"

	"runtime"

	"github.com/bwmarrin/discordgo"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/mem"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

type ServerInfo struct {
	uptime        time.Duration
	memory        *mem.VirtualMemoryStat
	cpuinfo       []float64
	numGoroutines int
	goMemStats    *runtime.MemStats
}

func SystemInfo(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "serverinfo") {
		info := getServerInfo()
		sendServerInfo(s, m, info)
	}
}

func getServerInfo() *ServerInfo {
	serverInfo := new(ServerInfo)
	if uptime, err := host.Uptime(); err == nil {
		d, _ := time.ParseDuration(fmt.Sprintf("%ds", uptime))
		serverInfo.uptime = d
	}
	if memory, err := mem.VirtualMemory(); err == nil {
		serverInfo.memory = memory
	}

	if cpupercent, err := cpu.Percent(1*time.Second, false); err == nil {
		serverInfo.cpuinfo = cpupercent
	}

	numGoroutines := runtime.NumGoroutine()
	serverInfo.numGoroutines = numGoroutines

	memStats := new(runtime.MemStats)
	runtime.ReadMemStats(memStats)
	serverInfo.goMemStats = memStats

	return serverInfo
}

func sendServerInfo(s *discordgo.Session, m *discordgo.MessageCreate, info *ServerInfo) {
	em := new(discordgo.MessageEmbed)
	em.Color = 0x87ceeb

	em.Title = "Server Stats"
	emuptme := &discordgo.MessageEmbedField{Name: "Uptime", Value: fmt.Sprint(info.uptime), Inline: true}

	emcpu := &discordgo.MessageEmbedField{Name: "Cpu Usage", Value: fmt.Sprintf("%f%%", info.cpuinfo[0]), Inline: true}

	emnumgoroutine := &discordgo.MessageEmbedField{Name: "Number Of Green Threads", Value: fmt.Sprint(info.numGoroutines), Inline: true}

	emMemStats := &discordgo.MessageEmbedField{Name: "Program Memory", Value: fmt.Sprintf("%dMB Used of %dMB Allocated, %f%% Used", bytesToMB(info.goMemStats.Alloc), bytesToMB(info.goMemStats.TotalAlloc), float64(info.goMemStats.Alloc)/float64(info.goMemStats.TotalAlloc)*100), Inline: false}

	emGarbage := &discordgo.MessageEmbedField{Name: "Garbage", Value: fmt.Sprintf("%dMB", bytesToMB(info.goMemStats.GCSys)), Inline: true}

	emmem := &discordgo.MessageEmbedField{Name: "Memory", Value: fmt.Sprintf("Total: %vMB, Free: %vMB, UsedPercent: %f%%\n", bytesToMB(info.memory.Total), bytesToMB(info.memory.Free), info.memory.UsedPercent)}
	em.Fields = []*discordgo.MessageEmbedField{emuptme, emcpu, emmem, emnumgoroutine, emGarbage, emMemStats}
	s.ChannelMessageSendEmbed(m.ChannelID, em)
}

func bytesToMB(bytes uint64) uint64 {
	return bytes / 1000000
}
