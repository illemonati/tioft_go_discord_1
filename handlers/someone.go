package handlers

import (
	"fmt"
	"log"
	"math/rand"
	"regexp"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

func Someone(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.MessageContains(m, "@someone") {
		channel, err := s.Channel(m.ChannelID)
		if err != nil {
			log.Println(err)
			return
		}
		users := channel.Recipients

		if len(users) < 1 {
			guild, err := s.Guild(m.GuildID)
			if err != nil {
				log.Println(err)
				return
			}
			for _, member := range guild.Members {
				users = append(users, member.User)
			}
			fmt.Println(users)
		}
		if len(users) < 1 {
			return
		}
		// replacedString := strings.Replace(m.Content, "@someone", users[rand.Intn(len(users))].Mention(), -1)

		regex := regexp.MustCompile("@someone")
		replacedString := regex.ReplaceAllStringFunc(m.Content, func(_ string) string {
			return users[rand.Intn(len(users))].Mention()
		})

		embed := new(discordgo.MessageEmbed)
		// embed.Title = fmt.Sprintf("%s#%s said", m.Author.Username, m.Author.Discriminator)
		embed.Description = replacedString
		footer := discordgo.MessageEmbedFooter{
			Text:    fmt.Sprintf("said %s#%s", m.Author.Username, m.Author.Discriminator),
			IconURL: m.Author.AvatarURL(""),
		}
		embed.Footer = &footer
		embed.Color = 0xF1C40F
		// s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("%s said\n>%s", m.Author.Mention(), replacedString))
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
	}
}
