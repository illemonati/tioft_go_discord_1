package handlers

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

func Avatar(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.MessageContains(m, "avatar") {
		if len(m.Mentions) < 1 {
			imgurl := m.Author.AvatarURL("1024")
			strings.Replace(imgurl, "webp", "png", -1)
			em := new(discordgo.MessageEmbed)
			emimg := new(discordgo.MessageEmbedImage)
			emimg.URL = imgurl
			em.Image = emimg
			em.Title = fmt.Sprintf("Avatar of %s", m.Author.Username)
			s.ChannelMessageSendEmbed(m.ChannelID, em)
		} else {
			mentions := m.Mentions
			for _, mention := range mentions {
				imgurl := mention.AvatarURL("1024")
				strings.Replace(imgurl, "webp", "png", -1)
				em := new(discordgo.MessageEmbed)
				emimg := new(discordgo.MessageEmbedImage)
				emimg.URL = imgurl
				em.Image = emimg
				em.Title = fmt.Sprintf("Avatar of %s", mention.Username)
				s.ChannelMessageSendEmbed(m.ChannelID, em)
			}
		}
	}
}
