package handlers

import (
	"fmt"
	"regexp"
	"strconv"
	"sync"

	"github.com/bwmarrin/discordgo"
	"github.com/parnurzeal/gorequest"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

func Scp(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "scp") {
		args := utils.GetMessageArguments(m)
		for _, arg := range args {
			n, err := strconv.Atoi(arg)
			if err != nil {
				continue
			}
			scp := newScp(n)
			scp.getInfo()
			em := new(discordgo.MessageEmbed)
			em.Title = fmt.Sprintf("SCP-%d (%s)", scp.itemN, scp.objectClass)
			em.URL = scp.url
			emp := new(discordgo.MessageEmbedField)
			emp.Name = "Procedure"
			emp.Value = scp.procedure
			emd := new(discordgo.MessageEmbedField)
			emd.Value = scp.description
			emd.Name = "Description"
			em.Fields = []*discordgo.MessageEmbedField{emp, emd}
			_, err = s.ChannelMessageSendEmbed(m.ChannelID, em)
		}
	}
}

type SCP struct {
	url, objectClass, description, procedure string
	itemN                                    int
}

func newScp(n int) *SCP {
	number := ""
	if n < 100 && n > 9 {
		number = fmt.Sprintf("0%d", n)
	} else {
		number = fmt.Sprintf("%d", n)
	}
	scpobj := new(SCP)
	scpobj.itemN = n
	scpobj.url = fmt.Sprintf("http://www.scp-wiki.net/scp-%s", number)
	return scpobj
}

func (scp *SCP) getInfo() {
	request := gorequest.New()
	_, body, _ := request.Get(scp.url).End()
	wg := new(sync.WaitGroup)
	wg.Add(3)
	go scp.getObjectClass(body, wg)
	go scp.getDescription(body, wg)
	go scp.getProcedure(body, wg)
	wg.Wait()
}

func (scp *SCP) getObjectClass(body string, wg *sync.WaitGroup) {
	reg := regexp.MustCompile(`Object Class:</strong>\s(?P<Class>\w+)`)
	captures := reg.FindStringSubmatch(body)
	if len(captures) >= 1 {
		scp.objectClass = utils.HtmlToText(captures[1])
	} else {
		scp.objectClass = "Not Found"
	}
	wg.Done()
}

func (scp *SCP) getDescription(body string, wg *sync.WaitGroup) {
	reg := regexp.MustCompile(`Description:</strong>(.+)</`)
	captures := reg.FindStringSubmatch(body)
	description := "N/A"
	if len(captures) >= 1 {
		description = utils.HtmlToText(captures[1])
	}
	if len(description) > 2047 {
		description = description[:2048]
	}
	scp.description = description
	wg.Done()
}

func (scp *SCP) getProcedure(body string, wg *sync.WaitGroup) {
	reg := regexp.MustCompile(`Special Containment Procedures:</strong>(.+)</`)
	captures := reg.FindStringSubmatch(body)
	procedure := "N/A"
	if len(captures) >= 1 {
		procedure = utils.HtmlToText(captures[1])
	}
	if len(procedure) > 2047 {
		procedure = procedure[:2048]
	}
	scp.procedure = procedure
	wg.Done()
}
