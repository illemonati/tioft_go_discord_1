package handlers

import (
	"fmt"
	"math/rand"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

func RandomCaps(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "randomCaps") {
		embed := new(discordgo.MessageEmbed)
		embed.Title = "Random Caps"
		actualContent := utils.StripFirstArgument(m)
		embed.Description = randomCapitalize(actualContent)
		footer := discordgo.MessageEmbedFooter{
			Text:    fmt.Sprintf("said %s#%s", m.Author.Username, m.Author.Discriminator),
			IconURL: m.Author.AvatarURL(""),
		}
		embed.Footer = &footer
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
	}
}

func randomCapitalize(s string) string {
	res := ""
	for _, c := range s {
		cs := string(c)
		f := rand.Float32()
		if f < 0.5 {
			res += strings.ToUpper(cs)
		} else {
			res += strings.ToLower(cs)
		}
	}
	return res
}
