package handlers

import (
	"github.com/bwmarrin/discordgo"
	"github.com/turnage/graw"
	"github.com/turnage/graw/reddit"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
	"log"
	"time"
)

var dogeHandler *DogeHandler = new(DogeHandler)

type DogeHandler struct {
	discordSession  *discordgo.Session
	discordChannels []string
	isRunning       bool
}

func (dh *DogeHandler) Post(post *reddit.Post) error {
	if post.URL != "" {
		for _, dc := range dh.discordChannels {
			_, err := dh.discordSession.ChannelMessageSendEmbed(dc, &discordgo.MessageEmbed{
				Title: post.Title,
				Image: &discordgo.MessageEmbedImage{
					URL: post.URL,
				},
			})
			if err != nil {
				log.Println(err)
			}
		}
	}
	return nil
}

func DogeLore(s *discordgo.Session, m *discordgo.MessageCreate) {

	dogeHandler.discordSession = s
	if !dogeHandler.isRunning {
		apiHandle, err := reddit.NewScript("Doge", 5*time.Second)
		if err != nil {
			log.Println(err)
			return
		}
		cfg := graw.Config{Subreddits: []string{"dogelore"}}
		_, wait, err := graw.Scan(dogeHandler, apiHandle, cfg)
		if err != nil {
			log.Println(err)
			return
		}
		go wait()
		dogeHandler.isRunning = true
		println("doge on")
	}

	insaved := false
	for _, c := range dogeHandler.discordChannels {
		if c == m.ChannelID {
			insaved = true
		}
	}
	if utils.IsCommand(m, "dogeOn") {
		if !insaved {
			dogeHandler.discordChannels = append(dogeHandler.discordChannels, m.ChannelID)
			log.Printf("dogeOn in channel %s", m.ChannelID)
			s.ChannelMessageSend(m.ChannelID, "doge on")
		}
	}

	if utils.IsCommand(m, "dogeOff") {
		toRemove := -1
		for i, channel := range dogeHandler.discordChannels {
			if channel == m.ChannelID {
				toRemove = i
			}
		}
		if toRemove != -1 {
			dogeHandler.discordChannels[toRemove] = dogeHandler.discordChannels[len(dogeHandler.discordChannels)-1]
			dogeHandler.discordChannels = dogeHandler.discordChannels[:len(dogeHandler.discordChannels)-1]
		}
		return
	}

}
