package handlers

import (
	"fmt"
	"log"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

const damedSmileComicBaseName = "damned-smile-images/page"
const damedSmileSmileImageName = "damned-smile-images/smile.png"

// DamnedSmile : Handler for DamnedSmile image and comic
func DamnedSmile(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "damnedSmile") {
		var files []*discordgo.File
		var err error
		if utils.GetMessageFirstArgument(m) == "full" {
			files, err = createDamnedSmileFiles()
		} else {
			var file *discordgo.File
			file, err = utils.GetAsset(damedSmileSmileImageName, "image/png")
			files = []*discordgo.File{file}
		}
		if err != nil {
			log.Println(err)
			return
		}
		ms := new(discordgo.MessageSend)
		ms.Files = files
		s.ChannelMessageSendComplex(m.ChannelID, ms)
	}
}

func createDamnedSmileFiles() (files []*discordgo.File, err error) {
	for i := 0; i < 8; i++ {
		filename := fmt.Sprintf("%s%d.jpg", damedSmileComicBaseName, i)
		var damnedSmilePage *discordgo.File
		damnedSmilePage, err = utils.GetAsset(filename, "image/jpg")
		if err != nil {
			log.Println(err)
			return
		}
		files = append(files, damnedSmilePage)
	}
	return
}
