package handlers

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

var imageFileName = "bestimage.jpg"

// TheBestImageEverCreated : Sends the best image ever created
func TheBestImageEverCreated(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "bestimage") {
		em := new(discordgo.MessageEmbed)
		em.Color = 0xFF69B4
		em.Title = "Best Image Ever Created"
		why1 := new(discordgo.MessageEmbedField)
		why2 := new(discordgo.MessageEmbedField)
		why3 := new(discordgo.MessageEmbedField)
		why4 := new(discordgo.MessageEmbedField)
		why1.Name = "Why - Reason 1"
		why1.Value = "This is the best simply because I belive it to be the best."
		why2.Name = "Why - Reason 2"
		why2.Value = "This image is uniquely creative, no one else has made something like it."
		why3.Name = "Why - Reason 3"
		why3.Value = `Alex said "dont ever send this again", so it's the duty of every citizen to send it as many times as they can.`
		why4.Name = "Why - Others"
		why4.Value = "The are infinitly many other reasons!"
		em.Fields = []*discordgo.MessageEmbedField{why1, why2, why3, why4}
		emim := new(discordgo.MessageEmbedImage)
		emim.URL = fmt.Sprintf("attachment://%s", imageFileName)
		em.Image = emim
		emFiles := []*discordgo.File{}
		image, err := utils.GetAsset(imageFileName, "image/jpg")
		if err != nil {
			fmt.Println(err)
			return
		}
		emFiles = append(emFiles, image)
		ms := discordgo.MessageSend{
			Content: "",
			Embed:   em,
			TTS:     true,
			Files:   emFiles,
		}
		s.ChannelMessageSendComplex(m.ChannelID, &ms)
	}
}
