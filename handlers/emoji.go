package handlers

import (
	"log"
	"path/filepath"
	"regexp"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

var emojiList = map[string]string{
	"bart": "bart.png",
	"bonk": "bonk.mp4",
}

var emojiImageExtensionRegex = regexp.MustCompile(`(jpg|png|jpeg|gif)`)

// Emoji : Handler for Emoji Arsenal
func Emoji(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "emoji") {
		name := ""

		if len(utils.GetMessageArguments(m)) < 2 {
			go emojiHelp(s, m)
			return
		}

		firstArg := utils.GetMessageFirstArgument(m)

		for key := range emojiList {
			if key == firstArg {
				name = key
				break
			}
		}

		if len(name) == 0 {
			go emojiHelp(s, m)
			return
		}

		go processEmoji(s, m, name)
	}
}

func processEmoji(s *discordgo.Session, m *discordgo.MessageCreate, name string) {
	fileName := emojiList[name]
	fileExtension := filepath.Ext(fileName)
	isImage := false
	if emojiImageExtensionRegex.MatchString(fileExtension) {
		isImage = true
	}
	files, err := createEmojiFiles(fileName, fileExtension, isImage)
	if err != nil {
		log.Println(err)
		return
	}
	ms := discordgo.MessageSend{
		Content: "",
		TTS:     true,
		Files:   files,
	}
	s.ChannelMessageSendComplex(m.ChannelID, &ms)
}

func createEmojiFiles(fileName, fileExtension string, isImage bool) (files []*discordgo.File, err error) {
	var emojiContent *discordgo.File

	if isImage {
		emojiContent, err = utils.GetAsset(fileName, "image/"+fileExtension)
	} else {
		emojiContent, err = utils.GetAsset(fileName, "video/"+fileExtension)
	}

	if err != nil {
		return
	}
	files = append(files, emojiContent)
	return
}

func emojiHelp(s *discordgo.Session, m *discordgo.MessageCreate) {
	embed := new(discordgo.MessageEmbed)
	embed.Title = "Emoji Help"

	usageField := new(discordgo.MessageEmbedField)
	usageField.Name = "Usage"
	usageField.Value = utils.PREFIX + "emoji <emoji name>"

	weaponNamesField := new(discordgo.MessageEmbedField)
	weaponNamesField.Name = "Emoji Names"
	weaponNamesField.Value = ""
	for name := range emojiList {
		weaponNamesField.Value += name + "\n"
	}

	embed.Fields = []*discordgo.MessageEmbedField{
		usageField,
		weaponNamesField,
	}

	s.ChannelMessageSendEmbed(m.ChannelID, embed)
}
