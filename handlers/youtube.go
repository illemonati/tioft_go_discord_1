package handlers

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"sync"

	"github.com/bwmarrin/dgvoice"
	"github.com/bwmarrin/discordgo"
	"github.com/kkdai/youtube/v2"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

const downloadCachePath = "./cache/youtube/"

var youtubeClient = youtube.Client{}

var voiceChannelStatuses = map[string]*VoiceChannelStatus{}

type VoiceChannelStatus struct {
	isPlaying          bool
	currentStopChannel chan bool
	queue              []*youtube.Video
	vc                 *discordgo.VoiceConnection
	mux                *sync.Mutex
}

// Youtube : Handler for youtube playback
func Youtube(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "yt") {
		k, _ := json.MarshalIndent(voiceChannelStatuses, "", "\n")
		log.Println(string(k))
		args := utils.GetMessageArguments(m)
		if utils.GetMessageFirstArgument(m) == "info" {
			if len(args) < 3 {
				return
			}
			link := args[2]
			video, err := getYoutubeVideo(link)
			if err != nil {
				log.Println(err)
				return
			}
			go sendYoutubeInfo(s, m, video)
			return
		}
		if utils.GetMessageFirstArgument(m) == "play" {
			if len(args) < 3 {
				return
			}
			link := args[2]
			video, err := getYoutubeVideo(link)
			if err != nil {
				return
			}
			go sendYoutubeInfo(s, m, video)
			_, vsChannelId := findVoiceChannel(s, m)
			if vsChannelId == "" {
				return
			}

			log.Println(vsChannelId)

			status, ok := voiceChannelStatuses[vsChannelId]
			if ok {
				status.mux.Lock()
				status.queue = append(status.queue, video)
				log.Println(status)
				status.mux.Unlock()
				if !status.isPlaying {
					go startPlay(s, m, video)
				}
			} else {
				voiceChannelStatuses[vsChannelId] = &VoiceChannelStatus{
					isPlaying:          true,
					currentStopChannel: nil,
					queue:              []*youtube.Video{video},
					mux:                new(sync.Mutex),
				}
				go startPlay(s, m, video)
			}
			return
		}
		if utils.GetMessageFirstArgument(m) == "skip" {
			skip(s, m)
			return
		}
		if utils.GetMessageFirstArgument(m) == "stop" {
			stop(s, m)
			return
		}
		if utils.GetMessageFirstArgument(m) == "queue" {
			sendQueue(s, m)
			return
		}
	}
}

func getYoutubeVideo(link string) (video *youtube.Video, err error) {
	video, err = youtubeClient.GetVideo(link)
	return
}

func getWebmFile(video *youtube.Video) (string, error) {
	if _, err := os.Stat(downloadCachePath); os.IsNotExist(err) {
		log.Println("creating youtube dir")
		os.MkdirAll(downloadCachePath, os.ModePerm)
	}
	webmpath := filepath.Join(downloadCachePath, fmt.Sprintf("%s.webm", video.ID))
	log.Println(webmpath)
	webmAudio := video.Formats.FindByItag(251)
	if _, err := os.Stat(webmpath); os.IsNotExist(err) {
		file, err := os.Create(webmpath)
		if err != nil {
			log.Println(err)
			return "", err
		}
		defer file.Close()
		stream, _, err := youtubeClient.GetStream(video, webmAudio)
		if err != nil {
			return "", err
		}
		_, err = io.Copy(file, stream)
		if err != nil {
			return "", err
		}
	}
	return webmpath, nil
}

func unpause(s *discordgo.Session, m *discordgo.MessageCreate) {
	_, vsChannelId := findVoiceChannel(s, m)
	if vsChannelId == "" {
		return
	}

	if status, ok := voiceChannelStatuses[vsChannelId]; ok {
		status.mux.Lock()
		status.currentStopChannel <- true
		status.mux.Unlock()
	}
}

func skip(s *discordgo.Session, m *discordgo.MessageCreate) {
	_, vsChannelId := findVoiceChannel(s, m)
	if vsChannelId == "" {
		return
	}

	log.Println(vsChannelId)

	if status, ok := voiceChannelStatuses[vsChannelId]; ok {
		log.Println(1)
		status.mux.Lock()
		status.currentStopChannel <- true
		status.mux.Unlock()
	}
}
func stop(s *discordgo.Session, m *discordgo.MessageCreate) {
	_, vsChannelId := findVoiceChannel(s, m)
	if vsChannelId == "" {
		return
	}

	log.Println(vsChannelId)

	if status, ok := voiceChannelStatuses[vsChannelId]; ok {
		status.mux.Lock()
		status.queue = []*youtube.Video{}
		status.currentStopChannel <- true
		status.isPlaying = false
		if status.vc != nil {
			status.vc.Disconnect()
		}
		status.mux.Unlock()
	}
}

func sendQueue(s *discordgo.Session, m *discordgo.MessageCreate) {
	_, vsChannelId := findVoiceChannel(s, m)
	if vsChannelId == "" {
		return
	}
	if status, ok := voiceChannelStatuses[vsChannelId]; ok {
		em := new(discordgo.MessageEmbed)
		em.Title = "Queue"
		em.Description = ""
		status.mux.Lock()
		for i, v := range status.queue {
			em.Description += fmt.Sprintf("%d) %s\n", i, v.Title)
		}
		status.mux.Unlock()
		_, err := s.ChannelMessageSendEmbed(m.ChannelID, em)
		log.Println(err)
	}

}

func startPlay(s *discordgo.Session, m *discordgo.MessageCreate, video *youtube.Video) {
	guildId, vsChannelId := findVoiceChannel(s, m)
	if vsChannelId == "" {
		return
	}
	if status, ok := voiceChannelStatuses[vsChannelId]; ok {
		status.mux.Lock()
		status.isPlaying = true
		status.mux.Unlock()
	} else {
		return
	}
	webmpath, err := getWebmFile(video)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(webmpath)

	vc, err := s.ChannelVoiceJoin(guildId, vsChannelId, false, true)
	if err != nil {
		return
	}
	stopChannel := make(chan bool)
	go handleAudioPlay(vc, webmpath, stopChannel, s, m)
	log.Println(vc.ChannelID)
	if status, ok := voiceChannelStatuses[vsChannelId]; ok {
		status.mux.Lock()
		status.currentStopChannel = stopChannel
		status.vc = vc
		status.mux.Unlock()
	} else {
		return
	}
	// log.Println(status.currentStopChannel)
	// log.Println(stopChannel)
	return
}

func handleAudioPlay(vc *discordgo.VoiceConnection, filepath string, stopChannel chan bool, s *discordgo.Session, m *discordgo.MessageCreate) {
	dgvoice.PlayAudioFile(vc, filepath, stopChannel)
	status, ok := voiceChannelStatuses[vc.ChannelID]
	if ok {
		status.mux.Lock()
		status.currentStopChannel = nil
		if len(status.queue) > 0 {
			status.queue = status.queue[1:]
		}
		if len(status.queue) > 0 {
			go startPlay(s, m, status.queue[0])
		} else {
			status.isPlaying = false
			if status.vc != nil {
				status.vc.Disconnect()
			}
		}
		status.mux.Unlock()
	}
}

func findVoiceChannel(s *discordgo.Session, m *discordgo.MessageCreate) (guildID string, channelID string) {
	c, err := s.State.Channel(m.ChannelID)
	if err != nil {
		// Could not find channel.
		return
	}

	// Find the guild for that channel.
	g, err := s.State.Guild(c.GuildID)
	if err != nil {
		// Could not find guild.
		return
	}

	// Look for the message sender in that guild's current voice states.
	for _, vs := range g.VoiceStates {
		if vs.UserID == m.Author.ID {
			guildID = c.GuildID
			channelID = vs.ChannelID
			return
		}
	}
	return
}

func sendYoutubeInfo(s *discordgo.Session, m *discordgo.MessageCreate, video *youtube.Video) {
	em := new(discordgo.MessageEmbed)
	em.Title = video.Title
	em.Fields = []*discordgo.MessageEmbedField{
		{
			Name:  "Author",
			Value: video.Author,
		},
		{
			Name:   "ID",
			Value:  video.ID,
			Inline: true,
		},
		{
			Name:   "Duration",
			Value:  fmt.Sprintf("%d", video.Duration),
			Inline: true,
		},
	}
	s.ChannelMessageSendEmbed(m.ChannelID, em)
}
