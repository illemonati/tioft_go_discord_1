package handlers

import (
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

// Latency : Measure Latency between discord message and processing
func Latency(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "latency") {
		latency, err := getLatency(m)
		sendLatency(s, m, latency, err)
	}
}

func getLatency(m *discordgo.MessageCreate) (int64, error) {
	now := time.Now()
	nsec := now.UnixNano()
	sentTime := m.Timestamp
	sentNsec := sentTime.UnixNano()
	latency := nsec - sentNsec
	return latency, nil
}

func sendLatency(s *discordgo.Session, m *discordgo.MessageCreate, latency int64, err error) {
	em := discordgo.MessageEmbed{
		Color: 0x87ceeb,
		Title: "Latency",
	}
	emLatencyField := discordgo.MessageEmbedField{
		Name:  "One Way Latency",
		Value: fmt.Sprintf("%f ms", float64(latency)/1e6),
	}
	em.Fields = []*discordgo.MessageEmbedField{
		&emLatencyField,
	}
	if err != nil {
		emErrField := discordgo.MessageEmbedField{
			Name:  "Error",
			Value: err.Error(),
		}
		em.Fields = append(em.Fields, &emErrField)
	}
	_, err = s.ChannelMessageSendEmbed(m.ChannelID, &em)
}
