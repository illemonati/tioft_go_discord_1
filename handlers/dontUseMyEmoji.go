package handlers

import (
	"math/rand"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

const BART_EMOJI = "<:bart:462986116090560512>"
const JACOB_ID = "313783057838768128"

func DontUseMyEmoji(s *discordgo.Session, m *discordgo.MessageCreate) {
	if strings.Contains(m.Content, BART_EMOJI) {
		if m.Author.ID == "313783057838768128" {
			s.ChannelTyping(m.ChannelID)
			rn := rand.Intn(60)
			for i := 0; i < rn; i += 10 {
				time.Sleep(time.Second * 10)
				s.ChannelTyping(m.ChannelID)
			}
			s.ChannelTyping(m.ChannelID)
			s.ChannelMessageSend(m.ChannelID, "use emoji")
		}
	}
}
