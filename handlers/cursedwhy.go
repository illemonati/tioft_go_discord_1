package handlers

import (
	"log"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

const cursedWhyGifName = "cursedwhy.gif"

// CursedWhy : Handler for the cursedWhy image
func CursedWhy(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "cursedwhy") {
		content := "Blame Alex"
		tts := true
		upload := false
		if utils.GetMessageFirstArgument(m) == "upload" {
			upload = true
		}
		embed := createCursedWhyEmbed(upload)
		files := []*discordgo.File{}
		if upload {
			var err error
			files, err = createCuredWhyFiles()
			if err != nil {
				log.Println(err)
				return
			}
		}

		ms := discordgo.MessageSend{
			Content: content,
			Embed:   embed,
			TTS:     tts,
			Files:   files,
		}

		s.ChannelMessageSendComplex(m.ChannelID, &ms)
	}
}

func createCursedWhyEmbed(upload bool) (embed *discordgo.MessageEmbed) {
	embed = new(discordgo.MessageEmbed)
	embed.Title = "Cursed Image"
	disclaimerField := new(discordgo.MessageEmbedField)
	disclaimerField.Name = "Disclaimer"
	disclaimerField.Value = "Just... Don't think about it"
	embed.Fields = []*discordgo.MessageEmbedField{disclaimerField}
	embed.Image = new(discordgo.MessageEmbedImage)
	if upload {
		embed.Image.URL = "attachment://" + cursedWhyGifName
	} else {
		embed.Image.URL = "https://i.imgur.com/grcb4o5.gif"
	}
	return
}

func createCuredWhyFiles() (files []*discordgo.File, err error) {
	cursedWhyGif, err := utils.GetAsset(cursedWhyGifName, "image/gif")
	if err != nil {
		return
	}
	files = append(files, cursedWhyGif)
	return
}
