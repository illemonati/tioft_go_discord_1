package handlers

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/illemonati/tioft_go_discord_1/utils"
)

func Ping(s *discordgo.Session, m *discordgo.MessageCreate) {
	if utils.IsCommand(m, "ping") {
		s.ChannelMessageSend(m.ChannelID, "<:bart:462986116090560512>")
	}
}
