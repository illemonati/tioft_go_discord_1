package utils

import (
	"encoding/json"
	"html"
	"os"
	"path"
	"strings"

	"github.com/bwmarrin/discordgo"
	strip "github.com/grokify/html-strip-tags-go"
)

// PREFIX : prefix
const PREFIX = "@@"

// AssetsDir : assets dir
const AssetsDir = "./assets/"

func GetAsset(filename, contentType string) (*discordgo.File, error) {
	file, err := os.Open(path.Join(AssetsDir, filename))
	if err != nil {
		return nil, err
	}
	discordFile := discordgo.File{
		Name:        filename,
		ContentType: contentType,
		Reader:      file,
	}
	return &discordFile, nil
}

func IsCommand(m *discordgo.MessageCreate, command string) bool {
	command = strings.ToLower(command)
	zeroth := strings.ToLower(GetMessageArguments(m)[0])
	return (zeroth == PREFIX+command)
}

func GetMessageFirstArgument(m *discordgo.MessageCreate) string {
	msg := m.Content
	msgParts := strings.Split(msg, " ")
	if len(msgParts) > 1 {
		return msgParts[1]
	}
	return ""
}

func GetMessageZerothArgument(m *discordgo.MessageCreate) string {
	msg := m.Content
	msgParts := strings.Split(msg, " ")
	return msgParts[0]
}

func GetMessageArguments(m *discordgo.MessageCreate) []string {
	msg := m.Content
	msgParts := strings.Split(msg, " ")
	return msgParts
}

func StripFirstArgument(m *discordgo.MessageCreate) string {
	parts := GetMessageArguments(m)
	return strings.Join(parts[1:], " ")
}

func MessageContains(m *discordgo.MessageCreate, contain string) bool {
	message := strings.ToLower(m.Content)
	contain = strings.ToLower(contain)
	return strings.Contains(message, contain)
}

func HtmlToText(input string) string {
	return html.UnescapeString(strip.StripTags(input))
}

func GuildEmojis(s *discordgo.Session, guildID string) (emojis []*discordgo.Emoji, err error) {

	body, err := s.RequestWithBucketID("GET", discordgo.EndpointGuildEmojis(guildID), nil, discordgo.EndpointGuildEmojis(guildID))
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &emojis)
	return
}

func FilterStringArray(ss []string, test func(string) bool) (ret []string) {
	for _, s := range ss {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return
}
