package utils

import (
	"errors"
	"os"
	"path"
	"path/filepath"

	"github.com/bwmarrin/discordgo"
	"github.com/parnurzeal/gorequest"
)

func DownloadFile(url string, filePath string) error {
	fileDirectory := filepath.Dir(filePath)
	os.MkdirAll(fileDirectory, os.ModePerm)
	_, body, errs := gorequest.New().Get(url).EndBytes()
	if errs != nil {
		return errs[0]
	}
	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer file.Close()
	file.Write(body)
	return nil
}

type DownloadAttachmentResult struct {
	AttachemntIndex int
	AttachmentURL   string
	FilePath        string
	Error           error
}

func AttachmentBaseDirToFilePath(baseDirectory string, attachment *discordgo.MessageAttachment) string {
	return filepath.Join(baseDirectory, attachment.ID, path.Base(attachment.URL))
}

func DownloadAttachments(m *discordgo.MessageCreate, baseDirectory string) chan DownloadAttachmentResult {
	resultsChan := make(chan DownloadAttachmentResult)
	for i, attachment := range m.Attachments {
		filePath := ""
		if len(baseDirectory) > 0 {
			filePath = AttachmentBaseDirToFilePath(baseDirectory, attachment)
		}
		go func(index int) {
			result := DownloadAttachment(m, index, filePath)
			resultsChan <- result
		}(i)
	}
	return resultsChan
}

func DownloadAttachment(m *discordgo.MessageCreate, index int, filePath string) DownloadAttachmentResult {

	if len(m.Attachments) < index+1 || index < 0 {
		return DownloadAttachmentResult{
			AttachemntIndex: index,
			AttachmentURL:   "",
			FilePath:        "",
			Error:           errors.New("Invalid attachment index"),
		}
	}

	attachment := m.Attachments[index]

	if len(filePath) == 0 {
		attachmentsDirectory := os.Getenv("DISCORD_ATTACHMENTS")
		if len(attachmentsDirectory) < 1 {
			attachmentsDirectory = "attachments"
		}
		filePath = AttachmentBaseDirToFilePath(attachmentsDirectory, attachment)
	}

	err := DownloadFile(attachment.URL, filePath)

	return DownloadAttachmentResult{
		AttachemntIndex: index,
		AttachmentURL:   attachment.URL,
		FilePath:        filePath,
		Error:           err,
	}
}
