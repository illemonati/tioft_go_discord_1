package utils

import (
	"encoding/json"
	"os"

	"github.com/bwmarrin/discordgo"
)

func LogMessage(m *discordgo.MessageCreate, downloadAttachments bool) {
	messageJSON, err := json.Marshal(m)
	if err != nil {
		return
	}
	if downloadAttachments {
		// go downloadAttachment(m)
		go DownloadAttachments(m, "")
	}
	filename := os.Getenv("DISCORD_LOG")
	if len(filename) < 1 {
		filename = "log.txt"
	}
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_RDWR|os.O_CREATE, 777)
	if err != nil {
		return
	}
	defer file.Close()
	file.WriteString(string(messageJSON) + "\n")
}

// func downloadAttachment(m *discordgo.MessageCreate) {
// 	attachmentsDirectory := os.Getenv("DISCORD_ATTACHMENTS")
// 	if len(attachmentsDirectory) < 1 {
// 		attachmentsDirectory = "attachments"
// 	}

// 	if _, err := os.Stat(attachmentsDirectory); os.IsNotExist(err) {
// 		os.Mkdir(attachmentsDirectory, os.ModePerm)
// 	}

// 	for _, attachment := range m.Attachments {
// 		attachmentDirectory := attachmentsDirectory + "/" + attachment.ID
// 		os.Mkdir(attachmentDirectory, os.ModePerm)
// 		_, body, errs := gorequest.New().Get(attachment.URL).EndBytes()
// 		if errs != nil {
// 			log.Println(errs)
// 			return
// 		}
// 		file, err := os.Create(attachmentDirectory + "/" + path.Base(attachment.URL))
// 		if err != nil {
// 			log.Println(err)
// 			return
// 		}
// 		defer file.Close()
// 		file.Write(body)

// 	}
// }
