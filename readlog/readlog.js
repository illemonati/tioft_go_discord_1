
const getLog = async (url, basicAuthUsername, basicAuthPassword) => {
    let headers = new Headers({
        'Content-Type': 'text/plain'
    }); 
    if (basicAuthUsername.length > 0) {
        headers.append('Authorization', `Basic ${btoa(basicAuthUsername + ':' + basicAuthPassword)}`);
    }
    const resp = await fetch(url, {
        method: 'GET',
        headers: headers
    });
    const log = await resp.text();
    return log
}

const getEntrys = async(baseurl, basicAuthUsername, basicAuthPassword) => {
    const url = baseurl + "\/log.txt";
    const log = await getLog(url, basicAuthUsername, basicAuthPassword);
    //const entrysOg = log.split(/.*(?=(\{\"id\".+\"cha))/g);
    const entrysOg = log.split('\n');
    const entrysRaw = entrysOg;
    //for (let i = 0; i < entrysOg.length; i++) {
    //    if ((i & 1) == 0) {
    //        entrysRaw.push(entrysOg[i]);
    //    } 
    //}
    const entrys = [];
    entrysRaw.forEach((e) => {
	try {
            obj = JSON.parse(e);
            entrys.push(obj);
        } catch (e) {}
    })
    return entrys.reverse();
}

const entryToDiv = (entry, attachmentdirUrl, loadImages) => {
    div = document.createElement('div');
    div.classList.add("messageDiv");
    div.innerHTML = `
        <h3> message id : ${entry.id} </h3>
        <p> channel id : ${entry.channel_id} </p>
        <p> guild id   : ${entry.guild_id} </p>
        <p> user id    : ${entry.author.id} </p>
        <p> username   : <span class="username"></span> </p>
        <p> user bot   : ${entry.author.bot} </p>
        <p> time stamp : ${entry.timestamp}  </p>
        <p class="message"></p>
    `;
    div.querySelector(".username").innerText = entry.author.username;
    div.querySelector(".message").innerText = entry.content;
    if (entry.content.length < 1) {
        div.querySelector(".message").classList.add("messageHidden");
    }

    if (entry.attachments.length > 0) {
        const attachmentsDiv = document.createElement('div');
        attachmentsDiv.classList.add("attachments");
        entry.attachments.forEach((attch) => {
            const attachmentDiv = document.createElement('div');
            attachmentDiv.innerHTML = `
                <p>Attachment Id : ${attch.id} </p>
                <a href=${attch.url} target="_blank" >${attch.filename}</a>
                <br />
                <a href=${attachmentdirUrl+'\/'+attch.id+'\/'+ attch.filename} target="_blank" >backup link</a>
            `;
            if ((/.*\.(png|jpg|webp|jpeg|gif)/g.test(attch.filename)) && (loadImages)){
                attachmentDiv.innerHTML += `
                    <br />
                    <img src="${attachmentdirUrl+'\/'+attch.id+'\/'+ attch.filename}" class="attachmentImage" />
                `
            }
            attachmentDiv.classList.add('attachment')
            attachmentsDiv.appendChild(attachmentDiv);
        });
        div.appendChild(attachmentsDiv);
    }

    return div;
}

const appendEntry = (entry, place, baseurl, loadImages) => {
    div = entryToDiv(entry, baseurl + '\/attachments\/', loadImages);
    place.appendChild(div);
}


const start = async () => {
    const urlInput = document.querySelector('#urlInput');
    const url = urlInput.value;
    const basicAuthUsername = document.querySelector("#basicAuthUsername").value;
    const basicAuthPassword = document.querySelector('#basicAuthPassword').value;
    const loadImages = document.querySelector('#loadImages').checked;
    const entrys = await getEntrys(url, basicAuthUsername, basicAuthPassword);
    const place = document.querySelector('#messages');
    place.innerHTML = '';
    entrys.forEach((e) => appendEntry(e, place, url, loadImages));
}



